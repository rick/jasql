﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("JaSql")]
[assembly: AssemblyDescription("A .NET library for using SQL - It is not an ORM, it is not a query builder. JaSql is a library that helps you keep SQL queries in one place and use them with ease.")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Rick van Lieshout")]
[assembly: AssemblyProduct("JaSql")]
[assembly: AssemblyCopyright("Copyright \u00A9 2014 Rick van Lieshout")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible
// to COM components.  If you need to access a type in this assembly from
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("d45c155e-ae88-42fd-950e-dc596931a13d")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]

[assembly: InternalsVisibleTo("JaSql.Tests")]
