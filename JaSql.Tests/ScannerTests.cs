﻿namespace JaSql.Tests
{
    using System.Collections.Generic;
    using System.IO;
    using System.Text;
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    [TestClass]
    public class ScannerTests
    {
        [TestMethod]
        public void GetTag()
        {
            var data = new Dictionary<string, string>
            {
                {"", null},
                {"-- Some Comment", null},
                {"-- name:     ", null},
                {"-- name: find-user-by-id", "find-user-by-id"},
                {"   -- name:      save-user     ", "save-user"},
            };

            foreach (var kv in data)
            {
                var r = Scanner.GetTag(kv.Key);
                Assert.AreEqual(kv.Value, r);
            }
        }

        [TestMethod]
        public void Run()
        {
            const string sqlFile = @"
-- name: all-users
-- Finds all users
SELECT * FROM users

-- name: empty-query-should-not-be-stored
-- name: save-user
INSERT INTO users (?, ?, ?)";

            var queries = new Scanner(new StreamReader(new MemoryStream(Encoding.UTF8.GetBytes(sqlFile ?? "")), Encoding.UTF8)).Run();

            Assert.IsNotNull(queries);
            Assert.AreEqual(2, queries.Count);
            Assert.IsTrue(queries.ContainsKey("all-users"));
            Assert.IsTrue(queries.ContainsKey("save-user"));
            Assert.IsFalse(queries.ContainsKey("empty-query-should-not-be-stored"));
        }
    }
}