﻿namespace JaSql.Tests
{
    using System.Collections.Generic;
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    [TestClass]
    public class JaSqlTests
    {
        [TestMethod]
        public void FactoryMethodsFromString()
        {
            const string sqlFile = @"
-- name: all-users
-- Finds all users
SELECT * FROM users

-- name: empty-query-should-not-be-stored
-- name: save-user
INSERT INTO users (?, ?, ?)";

            var jaSql = JaSql.FromString(sqlFile);

            Assert.IsNotNull(jaSql);
            Assert.IsNotNull(jaSql.Raw("all-users"));
            try
            {
                jaSql.Raw("empty-query-should-not-be-stored");
                Assert.Fail("Raw should not return without an exception when an invalid query name is specified.");
            }
            catch (KeyNotFoundException ex)
            {
                Assert.IsTrue(ex.Message.Contains("empty-query-should-not-be-stored"));
            }
        }

        [TestMethod]
        public void FactoryMethodsFromFile()
        {
            var jaSql = JaSql.FromFile(@"sql\test_schema.sql");

            Assert.IsNotNull(jaSql);
            Assert.IsNotNull(jaSql.Raw("create-users-table"));
        }
    }
}